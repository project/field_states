## Field States & transitions

Field States module is base on state machine module. Unlike the standard state
machine module, Field States offers an extended feature set. With this module,
you have the flexibility to define a list of states, within your field settings,
you can easily specify transitions.
This process becomes even more user-friendly, as you can conveniently copy and
paste configurations directly from your workflow YAML files.

Field States uses [https://mermaid.js](mermaid.js)  for visualizing your state machine,
making it easy to understand and manage your states and transitions.
Moreover, in the field formatter, you have the added convenience of selecting
transitions effortlessly, enhancing your overall workflow management experience.

### How to work
- Add field States transitions
- Fill all state in field storage (it looks like list string)
- set transitions in field settings (Yaml format)
- You can use diagram to draw state machine diagram after created the states.
Click on a state button & click on the next state, it'll show a line connecting
from state to the next state. There will show a button to transition the state,
you can change label, choose the actions when clicking on the button.

* label: Transition Button Name
* from: [Multi states] # The states from which this transition is allowed
* to: state # The state to which the field should transition
* role: The role name that can see the transition button
* permission: Permission can see button
* group: Group ID that User in group can see button
* attached: attached library
* guard: Guard Plugin ID in Your Custom Module (method allowed).
Custom guard logic if needed
* workflow: [Multi workflow id] in your custom module (method action).
Custom workflow logic if needed
* action: [Multi action id] in your custom module (method execute).
Custom action logic if needed

- guard: create custom/your_module/src/Plugin/Guard/CustomGuardPlugin.php
- workflow: create custom/your_module/src/Plugin/Guard/CustomWorkflowPlugin.php
- action: create custom/your_module/src/Plugin/Action/CustomAction.php
you can see example in module.
