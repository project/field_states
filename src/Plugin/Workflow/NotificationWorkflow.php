<?php

namespace Drupal\field_states\Plugin\Workflow;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\field_states\Attribute\Workflow;
use Drupal\field_states\WorkflowPluginBase;

/**
 * Plugin implementation of the workflow.
 */
#[Workflow(
  id: "notification_workflow",
  label: new TranslatableMarkup("Notification"),
  description: new TranslatableMarkup("Send notification description.")
)]
class NotificationWorkflow extends WorkflowPluginBase {

  use StringTranslationTrait;

  /**
   * Example methode.
   *
   * @param array $transition
   *   Transition current.
   * @param array $workflow
   *   Field settings configuration.
   * @param object $entity
   *   Entity.
   */
  public function action(&$transition, $workflow, &$entity) {
    // @phpstan-ignore-next-line
    $moduleHandler = \Drupal::service('module_handler');
    $uid = $entity->uid ?? FALSE;
    if ($uid && $moduleHandler->moduleExists('pwa_firebase')) {
      $title = $this->t('State change');
      $message = $this->t('From @from to @to', [
        '@from' => $transition['from'],
        '@to' => $transition['to'],
      ]);
      $url = $entity->toUrl();
      // @phpstan-ignore-next-line
      \Drupal::service('pwa_firebase.send')->sendMessageToUser($uid, $title, $message, $url);
    }
  }

}
