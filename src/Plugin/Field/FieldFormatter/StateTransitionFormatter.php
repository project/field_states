<?php

namespace Drupal\field_states\Plugin\Field\FieldFormatter;

use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityFormBuilderInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Field\FieldConfigInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\OptGroup;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\field_states\Form\StateTransitionForm;
use Drupal\options\Plugin\Field\FieldFormatter\OptionsDefaultFormatter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'State transition' formatter.
 */
#[FieldFormatter(
  id: 'state_transition',
  label: new TranslatableMarkup('State transition'),
  field_types: ['list_states'],
)]
class StateTransitionFormatter extends OptionsDefaultFormatter {

  /**
   * Constructs a new StateTransitionFormFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $classResolver
   *   The class resolver.
   * @param \Drupal\Core\Form\FormBuilderInterface $formBuilder
   *   The form builder.
   * @param \Drupal\Core\Entity\EntityFormBuilderInterface $entityFormBuilder
   *   The entity form build service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, protected ClassResolverInterface $classResolver, protected FormBuilderInterface $formBuilder, protected EntityFormBuilderInterface $entityFormBuilder, protected EntityTypeManagerInterface $entityTypeManager, protected EntityFieldManagerInterface $entityFieldManager, protected AccountInterface $currentUser) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('class_resolver'),
      $container->get('form_builder'),
      $container->get('entity.form_builder'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('current_user'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    $setting = [
      'require_confirmation' => FALSE,
      'use_modal' => FALSE,
      'history' => '',
      'show_state' => FALSE,
      'extras' => '',
    ];
    return $setting + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form = parent::settingsForm($form, $form_state);
    $form['require_confirmation'] = [
      '#title' => $this->t('Require confirmation before applying the state transition'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('require_confirmation'),
    ];

    $form['use_modal'] = [
      '#title' => $this->t('Display confirmation in a modal dialog'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('use_modal'),
      '#states' => [
        'visible' => [
          ':input[name*="require_confirmation"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $optionExtraFields = $options = [];
    $moduleSupport = ['string', 'json', 'double_field', 'triples_field'];
    $entity_type = $this->fieldDefinition->getTargetEntityTypeId();
    $bundle = $this->fieldDefinition->getTargetBundle();
    $fieldDefinitions = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle);
    foreach ($fieldDefinitions as $field_name => $field_definition) {
      if ($field_definition instanceof FieldConfigInterface &&
        in_array($field_definition->getType(), $moduleSupport)) {
        $options[$field_name] = (string) $field_definition->getLabel();
      }
      $optionExtraFields[$field_name] = (string) $field_definition->getLabel();
    }
    $form['history'] = [
      '#title' => $this->t('Field history'),
      '#description' => $this->t('Select field history to store.'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('history') ?? '',
      '#empty_option' => $this->t('- Select field -'),
      '#options' => $options,
    ];
    $form['extras'] = [
      '#title' => $this->t('Field extra'),
      '#description' => $this->t('Add extra field to transition form.'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('extras') ?? '',
      '#empty_option' => $this->t('- Select field -'),
      '#options' => $optionExtraFields,
    ];
    $form['show_state'] = [
      '#title' => $this->t('Display current state'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('show_state') ?? FALSE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {

    $summary = parent::settingsSummary();

    if ($this->getSetting('history')) {
      $summary[] = $this->t('History field: %history', ['%history' => $this->getSetting('history')]);
    }
    if ($this->getSetting('extras')) {
      $summary[] = $this->t('Extra field: %extras', ['%extras' => $this->getSetting('extras')]);
    }
    if ($this->getSetting('require_confirmation')) {
      $summary[] = $this->t('Require confirmation before applying the state transition.');

      if ($this->getSetting('use_modal')) {
        $summary[] = $this->t('Display confirmation in a modal dialog.');
      }
    }
    else {
      $summary[] = $this->t('Do not require confirmation before applying the state transition.');
    }
    if ($this->getSetting('show_state')) {
      $summary[] = $this->t('Display current state');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    /** @var \Drupal\Core\Entity\FieldableEntityInterface $entity */
    $entity = $items->getEntity();
    // Do not show the form if the user isn't allowed to modify the entity.
    if (!($this->currentUser->hasPermission('access states') || $entity->access('update'))) {
      return [];
    }
    $fieldDefinition = $items->getFieldDefinition();
    $fieldName = $fieldDefinition->getName();
    /** @var \Drupal\field_states\Form\StateTransitionFormInterface $form_object */
    $form_object = $this->classResolver->getInstanceFromDefinition(StateTransitionForm::class);
    $form_object->setEntity($entity);
    $form_object->setFieldName($fieldName);
    $form_state_additions = [
      'require_confirmation' => (bool) $this->getSetting('require_confirmation'),
      'use_modal' => (bool) $this->getSetting('use_modal'),
    ];
    if (!empty($this->getSetting('extras'))) {
      $form_state_additions['extras'] = $this->getSetting('extras');
      $form_mode = 'default';
      $entityForm = $this->entityFormBuilder->getForm($entity, $form_mode);
      $extras = explode(',', $form_state_additions['extras']);
      foreach ($extras as $extra) {
        if (!empty($entityForm[$extra])) {
          $form_state_additions['fields'][$extra] = $entityForm[$extra];
        }
      }
    }
    $form_state = (new FormState())->setFormState($form_state_additions);
    $elements = $this->formBuilder->buildForm($form_object, $form_state);

    if ($this->getSetting('show_state')) {
      $provider = $fieldDefinition->getFieldStorageDefinition()
        ->getOptionsProvider('value', $items->getEntity());
      // Flatten the possible options, to support opt groups.
      $options = OptGroup::flattenOptions($provider->getPossibleOptions());
      foreach ($items as $delta => $item) {
        $value = $item->value;
        // If the stored value is in the current set of allowed values, display
        // the associated label, otherwise just display the raw value.
        $output = $options[$value] ?? $value;
        $elements[$delta] = [
          '#type' => 'item',
          '#title' => $this->t('Current status'),
          '#markup' => $output,
          '#wrapper_attributes' => [
            'class' => ['state-status', $fieldName],
          ],
        ];
      }
    }

    if (!empty($elements['#empty_transition'])) {
      return parent::viewElements($items, $langcode);
    }
    return [$elements];
  }

}
