<?php

namespace Drupal\field_states\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\options\Plugin\Field\FieldFormatter\OptionsDefaultFormatter;

/**
 * Plugin implementation of the 'State default' formatter.
 */
#[FieldFormatter(
  id: 'state_default',
  label: new TranslatableMarkup('State default'),
  field_types: ['list_states'],
)]
class StateDefaultFormatter extends OptionsDefaultFormatter {

}
