<?php

namespace Drupal\field_states\Plugin\Field\FieldType;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Serialization\Yaml;
use Drupal\Component\Utility\Html;
use Drupal\Core\Field\Attribute\FieldType;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\options\Plugin\Field\FieldType\ListStringItem;
use Drupal\user\Entity\Role;

/**
 * Plugin implementation of the 'list_states' field type.
 */
#[FieldType(
  id: "list_states",
  label: new TranslatableMarkup("States transitions"),
  description: new TranslatableMarkup("Stores the current workflow state."),
  default_widget: "states_select",
  default_formatter: "state_transition",
  cardinality: 1,
)]
class ListStatesItem extends ListStringItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings(): array {
    $settings = ['workflows' => ''];
    return $settings + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state): array {
    $element = [];
    $stateDefault = $this->getValue();
    $workflow = $this->getWorkflow();
    $transitions = $this->getTransitions($workflow);
    $stateDiagram = ["stateDiagram-v2"];
    $userInput = $form_state->getUserInput();
    $triggering_element = $form_state->getTriggeringElement();
    $this->fieldStatesData($element);
    if (!empty($triggering_element) && isset($userInput["field_storage"]["subform"]["settings"]["allowed_values"]["table"])) {
      $states = array_reduce($userInput['field_storage']['subform']['settings']['allowed_values']['table'], function ($result, $item) {
        $result[$item['item']['key']] = $item['item']['label'];
        return $result;
      }, []);
      if (!empty($states)) {
        $workflow['states'] = $states;
      }
    }
    $allowed_values = $this->getSetting('allowed_values');
    if ($workflow["states"] != $allowed_values) {
      $workflow["states"] = $allowed_values;
    }
    $op = $userInput['op'] ?? FALSE;
    if (!empty($op) && $op == 'Save settings') {
      $allowedAdd = FALSE;
      if (!empty($states = $workflow["states"])) {
        foreach ($states as $state => $stateValue) {
          if (empty($allowed_values[$state])) {
            $allowed_values[$state] = $stateValue['label'];
            $allowedAdd = TRUE;
          }
        }
      }
      if (!empty($transitions)) {
        foreach ($transitions as $transition) {
          if (empty($allowed_values[$transition['to']])) {
            $allowed_values[$transition['to']] = $transition['to'];
            $allowedAdd = TRUE;
          }
        }
      }
      if ($allowedAdd) {
        $fieldName = $this->getFieldDefinition()->getName();
        $entityType = $this->getFieldDefinition()->getTargetEntityTypeId();
        $fieldStorage = FieldStorageConfig::loadByName($entityType, $fieldName);
        $fieldStorage->setSetting('allowed_values', $allowed_values);
        $fieldStorage->save();
      }
    }
    foreach ($allowed_values as $state => $stateName) {
      $state = str_replace('-', '_', HTML::getId($state));
      $stateName = str_replace(':', '', $stateName);
      $stateDiagram[] = "$state: $stateName";
    }
    if (!empty($stateDefault['value'])) {
      $stateDefault = $stateDefault['value'];
    }
    else {
      $stateDefault = '[*]';
    }
    if (!empty($transitions)) {
      foreach ($transitions as $transition) {
        foreach ($transition['from'] as $state) {
          if (empty($state)) {
            $state = $stateDefault;
          }
          else {
            $state = str_replace('-', '_', HTML::getId($state));
          }
          $to = !empty($transition['to']) ? HTML::getId($transition['to']) : '[*]';
          $query = trim($state) . ' --> ' . str_replace('-', '_', $to);
          if (!empty($transition['label'])) {
            $query .= ':' . $transition['label'];
          }
          $stateDiagram[] = $query;
        }
      }
    }

    $field_config = \Drupal::routeMatch()->getParameter('field_config');
    $element['diagram'] = [
      '#title' => $this->t('Diagram'),
      '#type' => 'details',
      '#open' => TRUE,
      'diagram' => [
        '#type' => 'container',
        '#title' => 'diagram',
        '#title_display' => 'invisible',
        '#attributes' => [
          'id' => 'edit-diagram',
          'data-node-spacing' => 150,
          'data-edge-spacing' => 150,
          'data-output' => 'edit-state',
          'data-format' => 'yaml',
          'edit-state' => 1,
        ],
      ],
    ];

    if ($field_config) {
      $element['diagram']['state_chart'] = [
        '#type' => 'link',
        '#url' => Url::fromRoute('field_states.state_machine', ['field' => $field_config->id()]),
        '#title' => $this->t('State diagram chart'),
      ];
    }

    $element['workflows'] = [
      '#title' => $this->t('Workflow'),
      '#type' => 'textarea',
      '#title_display' => 'invisible',
      '#default_value' => is_array($workflow) ? Yaml::encode($workflow) : $workflow,
      '#attributes' => [
        'id' => 'edit-state',
        'data-yaml-editor' => 'true',
        'data-editor' => 'settings-workflow',
        'class' => ['js-hide', 'yaml-editor'],
      ],
    ];
    $element['editor'] = [
      '#type' => 'details',
      '#title' => $this->t('Workflow advanced'),
      '#open' => TRUE,
      '#description' => $this->t('Define your workflow. You can empty to reset. Save and back for the first time.'),
      '#value' => [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['layout-row', 'clearfix', 'row'],
        ],
        'workflow' => [
          '#type' => 'container',
          '#description' => $this->t('Define your workflow'),
          '#attributes' => [
            'id' => 'settings-workflow',
            'class' => ['layout-column', 'layout-column--half', 'col-5'],
          ],
        ],
        'state-machine' => [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#attributes' => [
            'id' => 'stateDiagram',
            'class' => [
              'mermaid',
              'layout-column',
              'layout-column--half',
              'col-7',
            ],
          ],
          '#value' => implode("\n", $stateDiagram),
        ],
        'help' => [
          '#type' => 'link',
          '#url' => Url::fromRoute('help.page', ['name' => 'field_states']),
          '#title' => $this->t('List id: guard, workflow, action'),
          '#attributes' => [
            'class' => ['use-ajax'],
            'data-dialog-type' => 'dialog',
            'data-dialog-options' => Json::encode([
              'dialogClass' => 'token-tree-dialog',
              'width' => 650,
              'height' => 500,
              'draggable' => TRUE,
              'autoResize' => FALSE,
              'position' => [
                'my' => 'right bottom',
                'at' => 'right-10 bottom-10',
              ],
            ]),
          ],
        ],
      ],
    ];
    $form_state->setCached(FALSE);
    $form_state->setRebuild(TRUE);

    if ($field_config && count($field_config->getDefaultValueLiteral()) > 0) {
      $default_value = $field_config->getDefaultValueLiteral()[0];
      $element['#attached']['drupalSettings']['field_states']['default_value'] = $default_value['value'];
    }

    $element['#attached']['library'][] = 'field_states/state_machine_main';
    $element['#attached']['library'][] = 'field_states/field_states';
    return $element;
  }

  /**
   * Get transitions.
   *
   * {@inheritdoc}
   */
  public function getTransitions($workflow = []) {
    if (empty($workflow)) {
      $workflow = $this->getWorkflow();
    }
    return $workflow['transitions'] ?? [];
  }

  /**
   * Get workflow.
   *
   * {@inheritdoc}
   */
  public function getWorkflow() {
    $workflow = trim($this->getSetting('workflows') ?? '');
    if (empty($workflow)) {
      $workflow = $this->getWorkflowDefault();
    }
    if (is_string($workflow)) {
      $workflow = $this->handleRequired(Yaml::decode($workflow));
    }
    if (empty($workflow["transitions"]) || empty($workflow["states"])) {
      return $this->getWorkflowDefault();
    }
    return $workflow;
  }

  /**
   * If require is checked, it means there is no empty state.
   *
   * {@inheritdoc}
   */
  public function handleRequired($workflow) {
    $required = $this->getFieldDefinition()->isRequired();
    $states = $workflow['states'];
    $transitions = $workflow['transitions'] ?? [];

    if ($required) {
      foreach ($transitions as $key => $transition) {
        if (in_array("", $transition['from'], TRUE)) {
          unset($transitions[$key]);
        }
      }
    }
    else {
      $from = key(!empty($this->getValue()) ? $this->getValue() : $states);
      $transitionName = str_replace(' ', '_', $from ?? '');
      $transitionNew[$transitionName] = [
        'label' => trim(implode(' → ', ['', current($states)])),
        'from' => [''],
        'to' => array_key_first($states),
        'id' => $transitionName,
      ];
      $transitions = $transitionNew + $transitions;
    }

    $workflow['transitions'] = $transitions;
    return $workflow;
  }

  /**
   * Get transitions.
   *
   * {@inheritdoc}
   */
  public function getWorkflowDefault() {
    $states = $statesSetting = $this->getSetting('allowed_values');
    $from = '';
    $transitions = [];
    $required = $this->getFieldDefinition()->isRequired();
    foreach ($states as $state => $stateLabel) {
      if ($required && $from == '') {
        $from = $state;
        continue;
      }
      $transitionName = str_replace(' ', '_', "$from $state");
      $transitions[$transitionName] = [
        'label' => trim(implode(' → ', [$statesSetting[$from] ?? '', $stateLabel])),
        'from' => [$from],
        'to' => $state,
        'id' => $transitionName,
      ];
      $from = $state;
      $states[$state] = [
        'label' => $stateLabel,
      ];
    }
    return [
      'states' => $states,
      'transitions' => $transitions,
    ];
  }

  /**
   * Get data for transition.
   *
   * {@inheritdoc}
   */
  public function fieldStatesData(array &$element) {
    $guards = \Drupal::service('plugin.manager.states.guard')->getDefinitions();
    $workflows = \Drupal::service('plugin.manager.states.workflow')->getDefinitions();
    $actions = \Drupal::service('plugin.manager.action')->getDefinitions();
    $roles = Role::loadMultiple();
    sort($guards);
    sort($workflows);
    sort($actions);

    foreach ($roles as $role_id => $role) {
      $list_roles[$role_id] = $role->label();
    }

    foreach ($guards as $guard) {
      $list_guards[$guard['id']] = $guard['label'];
    }

    foreach ($workflows as $workflow) {
      $list_workflows[$workflow['id']] = $workflow['label'];
    }

    foreach ($actions as $action) {
      $list_actions[$action['id']] = $action['label'];
    }

    $data['roles'] = $list_roles ?? [];
    $data['guards'] = $list_guards ?? [];
    $data['workflows'] = $list_workflows ?? [];
    $data['actions'] = $list_actions ?? [];

    $element['#attached']['drupalSettings']['field_states']['diagram'] = $data;
  }

}
