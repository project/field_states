<?php

namespace Drupal\field_states\Plugin\Field\FieldWidget;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines the 'states_select' field widget.
 */
#[FieldWidget(
  id: 'states_select',
  label: new TranslatableMarkup('States list'),
  field_types: ['list_states'],
  multiple_values: TRUE,
)]
class StatesSelectWidget extends OptionsSelectWidget {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'transition' => TRUE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['transition'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use transition for list state'),
      '#default_value' => $this->getSetting('transition'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    if (!empty($this->getSetting('transition'))) {
      $summary[] = $this->t('Use transition for list state');
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    // @phpstan-ignore-next-line
    $passPermission = \Drupal::currentUser()->hasPermission('access states');
    if ($passPermission) {
      return $element;
    }
    $useTransition = $this->getSetting('transition');
    if ($useTransition) {
      $state = $items->value;
      $workflow = $items->getFieldDefinition()->getSetting('workflows');
      $workflow = Yaml::decode($workflow);
      $transitions = $workflow['transitions'] ?? [];
      $stateOption = [$state];
      foreach ($transitions as $transition) {
        if (in_array($state, $transition['from'])) {
          $stateOption[] = $transition['to'];
        }
      }
      $optionOrigin = $element["#options"];
      foreach ($element["#options"] as $state => $stateLabel) {
        if (!in_array($state, $stateOption)) {
          unset($element["#options"][$state]);
        }
      }
      if (empty($element["#options"])) {
        $element["#options"] = $optionOrigin;
      }
      $element["#empty_option"] = $this->t('Select next state');
    }
    return $element;
  }

}
