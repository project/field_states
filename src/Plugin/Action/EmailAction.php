<?php

namespace Drupal\field_states\Plugin\Action;

use Drupal\Core\Action\ActionBase;
use Drupal\Core\Action\Attribute\Action;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\Entity\User;

/**
 * Plugin implementation of the workflow.
 */
#[Action(
  id: 'email_action',
  label: new TranslatableMarkup('Send email'),
  action_label: new TranslatableMarkup('Send email'),
)]
class EmailAction extends ActionBase {

  /**
   * Example methode execute. It must have entity, transition, workflow.
   */
  public function execute($entity = NULL, $transition = [], $workflow = []) {
    $uid = $entity->getOwnerId() ?? FALSE;
    if ($uid) {
      $author = User::load($uid);
      $mailManager = \Drupal::service('plugin.manager.mail');
      $to = $author->getEmail();
      $key = 'change_state';
      $module = 'field_states';
      $langcode = $author->getPreferredLangcode();
      $send = TRUE;
      $url = $entity->toUrl()->setAbsolute()->toString();
      $body = $this->t('From @from to @to', [
        '@from' => $transition['state'],
        '@to' => $transition['to'],
      ])->render();
      $message['from'] = \Drupal::config('system.site')->get('mail');
      $message['subject'] = $this->t('State change');
      $message['body'][] = $body . ' ' . $url;
      $result = $mailManager->mail($module, $key, $to, $langcode, $message, NULL, $send);
      if ($result['result'] != TRUE) {
        $message = $this->t('There was a problem sending your email notification to @email.', ['@email' => $to]);
        \Drupal::messenger()->addMessage($message, 'error');
        \Drupal::logger('mail-log')->error($message);
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function access($object, ?AccountInterface $account = NULL, $return_as_object = FALSE) {
    // @todo Fix custom access() method.
  }

}
