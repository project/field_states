<?php

namespace Drupal\field_states\Plugin\Guard;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\field_states\Attribute\Guard;
use Drupal\field_states\GuardPluginBase;

/**
 * Plugin implementation of the guard.
 */
#[Guard(
  id: "user_guard",
  label: new TranslatableMarkup("User guard"),
  description: new TranslatableMarkup("User guard description.")
)]
class UserGuard extends GuardPluginBase {

  /**
   * Example method.
   */
  public function allowed(&$transition, $workflow, $entity = NULL) {
    // @todo Implement allowed() method.
    $transition['class'] = 'btn btn-success';
    $transition['attributes'] = [
      'data-bs-toggle' => "tooltip",
      'data-id' => $entity->id(),
    ];
    return TRUE;
  }

}
