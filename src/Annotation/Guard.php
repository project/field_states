<?php

namespace Drupal\field_states\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines guard annotation object.
 *
 * @Annotation
 */
class Guard extends Plugin {

  /**
   * The plugin ID.
   */
  public string $id;

  /**
   * The human-readable name of the plugin.
   *
   * @ingroup plugin_translatable
   */
  public string $label;

  /**
   * The description of the plugin.
   *
   * @ingroup plugin_translatable
   */
  public string $description;

}
