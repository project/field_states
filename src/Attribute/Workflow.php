<?php

namespace Drupal\field_states\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines workflow annotation object.
 *
 * @Annotation
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class Workflow extends Plugin {

  /**
   * The constructor workflow state field.
   *
   * @param string $id
   *   The plugin ID.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $label
   *   The name of the fieldgroup formatter class.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $description
   *   The description of the fieldgroup formatter class.
   */
  public function __construct(
    public readonly string $id,
    public readonly TranslatableMarkup $label,
    public readonly TranslatableMarkup $description,
  ) {}

}
