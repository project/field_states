<?php

namespace Drupal\field_states;

/**
 * Interface for States and transitions.
 */
interface StatesTransitionInterface {

  /**
   * Set transitions.
   *
   * @param array|mixed $transitions
   *   Transitions.
   *
   * @return mixed
   *   Return this.
   */
  public function setTransitions($transitions);

  /**
   * Get transitions.
   *
   * @return array
   *   Return transitions.
   */
  public function getTransitions();

  /**
   * Get transition from a status to a status.
   *
   * @param string $fromState
   *   From state.
   * @param string $toState
   *   To state.
   *
   * @return mixed
   *   Return this.
   */
  public function transition($fromState, $toState);

  /**
   * Execute login in transition.
   *
   * @return mixed
   *   Return this.
   */
  public function applyTransition(&$entity = NULL, $field_name = NULL);

  /**
   * Check current transition is allowed to execute.
   *
   * @param null|array $transition
   *   Transition.
   * @param null|object $entity
   *   Entity.
   *
   * @return bool
   *   Return this.
   */
  public function isTransitionAllowed(&$transition = NULL, $entity = NULL);

  /**
   * Set transitions by field.
   *
   * @param object $entity
   *   Entity.
   * @param string $field_name
   *   Field name.
   */
  public function setTransitionByField($entity, $field_name);

  /**
   * Get all transitions with from state.
   *
   * @param string $state
   *   State.
   */
  public function getPossibleTransitions($state);

}
