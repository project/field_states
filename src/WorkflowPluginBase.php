<?php

namespace Drupal\field_states;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for workflow plugins.
 */
abstract class WorkflowPluginBase extends PluginBase implements WorkflowInterface {

}
