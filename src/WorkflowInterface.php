<?php

namespace Drupal\field_states;

/**
 * Interface for workflow plugins.
 */
interface WorkflowInterface {

  /**
   * Returns the plugin action.
   *
   * @param array $transition
   *   Transition.
   * @param array $workflow
   *   WorkFlow.
   * @param object $entity
   *   Entity.
   */
  public function action(&$transition, $workflow, &$entity);

}
