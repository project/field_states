<?php

namespace Drupal\field_states;

/**
 * Interface for guard plugins.
 */
interface GuardInterface {

  /**
   * Returns the plugin allowed.
   */
  public function allowed(&$transition, $workflow, $entity);

}
