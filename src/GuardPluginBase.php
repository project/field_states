<?php

namespace Drupal\field_states;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for guard plugins.
 */
abstract class GuardPluginBase extends PluginBase implements GuardInterface {

}
