<?php

namespace Drupal\field_states;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\field_states\Attribute\Guard;

/**
 * Guard plugin manager.
 */
final class GuardPluginManager extends DefaultPluginManager {

  /**
   * Constructs GuardPluginManager object.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Guard',
      $namespaces,
      $module_handler,
      'Drupal\field_states\GuardInterface',
      Guard::class,
      'Drupal\field_states\Annotation\Guard'
    );
    $this->alterInfo('guard_info');
    $this->setCacheBackend($cache_backend, 'guard_plugins');
  }

}
