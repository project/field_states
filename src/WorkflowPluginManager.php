<?php

namespace Drupal\field_states;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\field_states\Attribute\Workflow;

/**
 * Workflow plugin manager.
 */
final class WorkflowPluginManager extends DefaultPluginManager {

  /**
   * Constructs WorkflowPluginManager object.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Workflow',
      $namespaces,
      $module_handler,
      'Drupal\field_states\WorkflowInterface',
      Workflow::class,
      'Drupal\field_states\Annotation\Workflow'
    );
    $this->alterInfo('workflow_info');
    $this->setCacheBackend($cache_backend, 'workflow_plugins');
  }

}
