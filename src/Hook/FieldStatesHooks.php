<?php

namespace Drupal\field_states\Hook;

use Drupal\Core\Field\FieldTypeCategoryManagerInterface;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\user\Entity\Role;

/**
 * Hook implementations for field states.
 */
class FieldStatesHooks {
  use StringTranslationTrait;

  /**
   * Implements hook_help().
   */
  #[Hook('help')]
  public function help($route_name, RouteMatchInterface $route_match) {
    switch ($route_name) {
      case 'help.page.field_states':
        $renderer = \Drupal::service('renderer');
        $guards = array_keys(\Drupal::service('plugin.manager.states.guard')->getDefinitions());
        $workflows = array_keys(\Drupal::service('plugin.manager.states.workflow')->getDefinitions());
        $actions = array_keys(\Drupal::service('plugin.manager.action')->getDefinitions());
        sort($guards);
        sort($workflows);
        sort($actions);
        $guardsList = [
          '#theme' => 'item_list',
          '#list_type' => 'ul',
          '#title' => $this->t('Guard id List'),
          '#items' => $guards,
        ];
        $workflowsList = [
          '#theme' => 'item_list',
          '#list_type' => 'ul',
          '#title' => $this->t('Workflow id List'),
          '#items' => $workflows,
        ];
        $actionsList = [
          '#theme' => 'item_list',
          '#list_type' => 'ul',
          '#title' => $this->t('Action id List'),
          '#items' => $actions,
        ];
        $roles = array_keys(Role::loadMultiple());
        $roleList = [
          '#theme' => 'item_list',
          '#list_type' => 'ul',
          '#title' => $this->t('Role id List'),
          '#items' => $roles,
        ];
        $output = '<p>' . $this->t('Base on state machine module. But you have the flexibility to define a list of states & transitions within field settings. This process becomes even more user-friendly, as you can conveniently copy and paste configurations directly from your workflow YAML files.') . '</p>';
        $output .= '<p>' . $this->t('Field States uses mermaid.js for visualizing your state machine, making it easy to understand and manage your states and transitions. You can define your states and transitions like this:') . '</p>';
        $output .= '<pre>transitions: <br/> transition_name:</br>  label: Name<br/>  from: [state]<br/>  to: state<br/>  class: button class example btn-danger<br/>  guard: [guardId]<br/>  role: [roleId]<br/>  workflow: [workflowId]<br/>  action: [actionId]</pre>';
        $output .= $renderer->render($roleList);
        $output .= $renderer->render($guardsList);
        $output .= $renderer->render($workflowsList);
        $output .= $renderer->render($actionsList);
        return $output;

      default:
        break;
    }
    return FALSE;

  }

  /**
   * Implements hook_theme().
   */
  #[Hook('theme')]
  public function theme(): array {
    return [
      'state_diagram' => [
        'variables' => [
          'title' => 'State diagram',
          'description' => '',
          'nodeSpacing' => 150,
          'edgeSpacing' => 150,
          'fieldName' => '',
          'output' => 'edit-state',
          'format' => 'yaml',
          'editState' => FALSE,
        ],
      ],
    ];
  }

  /**
   * Implements hook_field_type_category_info_alter().
   */
  #[Hook('field_type_category_info_alter')]
  public function fieldTypeCategoryInfoAlter(&$definitions): void {
    // The `comment` field type belongs in the `general` category, so the
    // libraries need to be attached using an alter hook.
    $definitions[FieldTypeCategoryManagerInterface::FALLBACK_CATEGORY]['libraries'][] = 'field_states/drupal.field-state-icon';
  }

  /**
   * Implements hook_entity_type_alter().
   */
  #[Hook('entity_type_alter')]
  public function entityTypeAlter(array &$entity_types) : void {
    /** @var \Drupal\Core\Entity\EntityTypeInterface[] $entity_types */
    foreach ($entity_types as $entity_type) {
      /** @var \Drupal\Core\Entity\EntityTypeInterface $entity_type */
      if ($entity_type->hasLinkTemplate('edit-form')) {
        $entity_type->setLinkTemplate('transition-form', $entity_type->getLinkTemplate('edit-form') . '/transition/{field_name}/{transition_id}');
      }
    }
  }

}
