<?php

namespace Drupal\field_states\Form;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field_states\StatesTransitionService;

/**
 * Transition form.
 */
class StateTransitionForm extends FormBase implements StateTransitionFormInterface {

  /**
   * The entity.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface
   */
  protected $entity;

  /**
   * The state field name.
   *
   * @var string
   */
  protected $fieldName;

  /**
   * Constructs a transition form object.
   *
   * @param \Drupal\field_states\StatesTransitionService $stateService
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $dateFormatter
   *   The date formatter service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   */
  public function __construct(protected StatesTransitionService $stateService, protected AccountInterface $currentUser, protected DateFormatterInterface $dateFormatter, protected EntityTypeManagerInterface $entityTypeManager) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create($container) {
    return new static(
      $container->get('field_states.transitions'),
      $container->get('current_user'),
      $container->get('date.formatter'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setEntity(ContentEntityInterface $entity) {
    $this->entity = $entity;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldName() {
    return $this->fieldName;
  }

  /**
   * {@inheritdoc}
   */
  public function setFieldName($field_name) {
    $this->fieldName = $field_name;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseFormId() {
    return 'state_machine_transition_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    $entity = $this->getEntity();
    if (!$entity) {
      throw new \RuntimeException('No entity provided to StateTransitionForm.');
    }
    // Example ID: "state_machine_transition_form_commerce_order_state_1".
    $form_id = $this->getBaseFormId();
    $form_id .= '_' . $entity->getEntityTypeId() . '_' . $this->fieldName;
    $form_id .= '_' . $entity->id();

    return $form_id;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\state_machine\Plugin\Field\FieldType\StateItemInterface $state_item */
    $state_item = $this->entity->get($this->fieldName)->first();
    $state = $state_item ? $state_item->get('value')->getString() : '';
    $transitions = $this->stateService
      ->setTransitionByField($this->entity, $this->fieldName)
      ->getPossibleTransitions($state);
    $weight = 0;
    $extra = FALSE;
    $storage = $form_state->getStorage();
    if (!empty($storage['fields'])) {
      $extra = TRUE;
      $weights = array_column($storage['fields'], '#weight');
      $weight = !empty($weights) ? max(array_filter($weights, fn($w) => !empty($w))) : 0;
    }
    $form['actions'] = [
      '#type' => 'container',
      '#weight' => ++$weight,
    ];
    foreach ($transitions->getTransitions() as $transition_id => $transition) {
      if (empty($transition['id'])) {
        $transition['id'] = $transition_id;
      }
      $allowed = $transitions->isTransitionAllowed($transition, $this->entity);
      if (!$allowed) {
        continue;
      }
      $form['actions'][$transition_id] = [
        '#type' => 'submit',
        '#value' => $transition['label'],
        '#submit' => ['::submitForm'],
        '#transition' => $transition,
      ];
      if (!empty($transition['attributes'])) {
        $form['actions'][$transition_id]['#attributes'] = $form['actions'][$transition_id]['#attributes'] ?? [];
        $form['actions'][$transition_id]['#attributes'] += $transition['attributes'];
      }
      if (!empty($transition['class'])) {
        $form['actions'][$transition_id]['#attributes']['class'] = is_array($transition['class']) ? $transition['class'] : explode(' ', $transition['class']);
      }
      if (!empty($transition['attached'])) {
        if (is_string($transition['attached'])) {
          $form['#attached']['library'][] = $transition['attached'];
        }
        elseif (is_array($transition['attached'])) {
          $form['#attached']['library'] = [...$form['#attached']['library'], ...$transition['attached']];
        }
      }
      if (isset($transition['extra']) && !$transition['extra']) {
        $extra = FALSE;
      }
    }
    // Add extra field to form.
    if ($extra) {
      $form += $storage['fields'];
    }
    if (empty($transitions)) {
      $form['#empty_transition'] = TRUE;
      $form['#state'] = $state;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $transition = $triggering_element['#transition'];
    $storage = $form_state->getStorage();
    $values = $form_state->getValues();
    if (!empty($storage['extras'])) {
      $extras = explode(',', $storage['extras']);
      foreach ($extras as $extra) {
        $extra = trim($extra);
        if ($this->entity->hasField($extra)) {
          $this->entity->get($extra)->setValue($values[$extra] ?? NULL);
        }
      }
    }
    // Save log.
    $properties = [
      'targetEntityType' => $this->getEntity()->getEntityTypeId(),
      'bundle' => $this->getEntity()->bundle(),
    ];
    $view_displays = $this->entityTypeManager->getStorage('entity_view_display')
      ->loadByProperties($properties);
    $view_display = current($view_displays);
    $component = $view_display->getComponent($this->fieldName);
    $fieldSettings = $component['settings'] ?? ['history' => FALSE];
    $transitions = $this->stateService->setTransitionByField($this->entity, $this->fieldName)->setTransition($transition);
    $this->entity->get($this->fieldName)->setValue(['value' => $transition['to']]);
    $transitions->applyTransition($this->entity, $this->fieldName);
    if (!empty($fieldSettings['history'])) {
      $history = $fieldSettings['history'];
      $field_instance = FieldConfig::loadByName($properties['targetEntityType'], $properties['bundle'], $history);
      $fieldType = $field_instance->getFieldStorageDefinition()->getType();

      $timestamp = strtotime('now');
      $log = [
        'date' => $this->dateFormatter->format($timestamp),
        'from' => $transition["state"] ?? '',
        'to' => $transition["to"] ?? '',
        'user' => $this->currentUser->getDisplayName(),
      ];
      $to = ' → ';
      switch ($fieldType) {
        case 'json':
          $value = $this->entity->get($history)->value;
          $json = json_decode($value ?: '[]');
          $json[] = $log;
          $this->entity->set($history, json_encode($json));
          break;

        case 'string':
          $this->entity->$history->appendItem(
            $log['date'] . ' State: ' . $log['from'] . $to . $log['to'] . ' by: ' . $log['user']
          );
          break;

        case 'double_field':
          $this->entity->$history->appendItem([
            'first' => date('Y-m-d\TH:i:s'),
            'second' => $log['from'] . $to . $log['to'] . ' - ' . $log['user'],
          ]);
          break;

        case 'triples_field':
          $this->entity->$history->appendItem([
            'first' => date('Y-m-d\TH:i:s'),
            'second' => implode($to, [$log['from'], $log['to']]),
            'third' => $log['user'],
          ]);
          break;

      }
    }
    if (!empty($transition['attached'])) {
      $form['#attached']['library'][] = $transition['attached'];
    }
    $inputValues = $form_state->getUserInput();
    foreach ($inputValues as $field_name => $value) {
      if ($this->entity->hasField($field_name) && $value !== NULL) {
        $this->entity->set($field_name, $value);
      }
    }
    if (!empty($this->entity) && method_exists($this->entity, 'save')) {
      $this->entity->save();
    }
    if (!empty($transition['redirect'])) {
      $form_state->setRedirect($transition['redirect']);
    }
  }

}
