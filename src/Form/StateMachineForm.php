<?php

namespace Drupal\field_states\Form;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field_states\GuardPluginManager;
use Drupal\field_states\WorkflowPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Provides a Field states form.
 */
class StateMachineForm extends FormBase {

  /**
   * Constructor with property promotion in PHP 8.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   Cache service.
   * @param \Drupal\field_states\GuardPluginManager $guardManager
   *   The field state guard service.
   * @param \Drupal\field_states\WorkflowPluginManager $workflowManager
   *   The field state workflow service.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $actionManager
   *   The action service.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   Current user service.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected CacheBackendInterface $cacheBackend,
    protected GuardPluginManager $guardManager,
    protected WorkflowPluginManager $workflowManager,
    protected PluginManagerInterface $actionManager,
    protected AccountInterface $currentUser,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('cache.render'),
      $container->get('plugin.manager.states.guard'),
      $container->get('plugin.manager.states.workflow'),
      $container->get('plugin.manager.action'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'field_states_state_machine';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $field = NULL): array {
    $field_config = NULL;
    if (!empty($field)) {
      [$entity_type, $bundle, $field_name] = explode('.', $field);
      $field_config = FieldConfig::loadByName($entity_type, $bundle, $field_name);
      $field_settings = $field_config->getSettings();
      $form_state->set('field', $field);
    }

    $this->fieldStatesData(form: $form);

    $form['diagram'] = [
      '#theme' => 'state_diagram',
      '#description' => $field_config->getDescription(),
      '#nodeSpacing' => 150,
      '#edgeSpacing' => 150,
      '#output' => 'edit-state',
      '#format' => 'yaml',
      '#fieldName' => $field,
      '#attached' => [
        'drupalSettings' => [
          'field_states' => [
            'fieldName' => $field,
          ],
        ],
      ],
      '#editState' => $this->currentUser->hasPermission('Administer transition states'),
    ];

    $form['#title'] = $field_config?->label();

    $form['state'] = [
      '#type' => 'textarea',
      '#attributes' => [
        'class' => ['js-hide'],
      ],
      '#default_value' => $field_settings["workflows"] ?? '',
    ];

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Save'),
      ],

    ];

    $form['#attached']['library'] = [
      'field_states/state_machine_main',
    ];

    if (count($field_config->getDefaultValueLiteral()) > 0) {
      $default_value = $field_config->getDefaultValueLiteral()[0];
      $form['#attached']['drupalSettings']['field_states']['default_value'] = $default_value['value'];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {

  }

  /**
   * {@inheritdoc}
   */
  protected function fieldStatesData(array &$form) {
    $guards = $this->guardManager->getDefinitions();
    $workflows = $this->workflowManager->getDefinitions();
    $actions = $this->actionManager->getDefinitions();
    $roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();
    sort($guards);
    sort($workflows);
    sort($actions);

    foreach ($roles as $role_id => $role) {
      $list_roles[$role_id] = $role->label();
    }

    foreach ($guards as $guard) {
      $list_guards[$guard['id']] = $guard['label'];
    }

    foreach ($workflows as $workflow) {
      $list_workflows[$workflow['id']] = $workflow['label'];
    }

    foreach ($actions as $action) {
      $list_actions[$action['id']] = $action['label'];
    }

    $data['roles'] = $list_roles ?? [];
    $data['guards'] = $list_guards ?? [];
    $data['workflows'] = $list_workflows ?? [];
    $data['actions'] = $list_actions ?? [];

    $form['#attached']['drupalSettings']['field_states']['diagram'] = $data;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {

    $config_value = $form_state->getValue('state');
    $field = $form_state->get('field');

    if (!empty($field)) {
      $array_value = Yaml::parse($config_value);
      $configFactory = $this->configFactory()->getEditable('field.field.' . $field);

      $configFactory->set('settings', [
        'workflows' => $config_value,
        'allowed_values' => $array_value['states'],
      ])->save(TRUE);
      $this->cacheBackend->invalidateAll();
      $this->messenger()->addStatus($this->t('Configuration has been saved.'));
    }
    else {
      $this->messenger()->addError($this->t('Not found %field', ['%field' => $field]));
    }
  }

}
