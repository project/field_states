<?php

namespace Drupal\field_states;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Action\ActionManager;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\group\Entity\Group;

/**
 * Guard plugin manager.
 */
class StatesTransitionService implements StatesTransitionInterface {

  /**
   * The transitions.
   *
   * @var array
   */
  protected array $transitions = [];

  /**
   * The transition.
   *
   * @var array|mixed
   */
  protected mixed $transition = NULL;

  /**
   * Constructs a States Transitions Service object.
   */
  public function __construct(
    protected GuardPluginManager $guard,
    protected WorkflowPluginManager $workflow,
    protected ActionManager $action,
    protected AccountProxyInterface $currentUser,
  ) {
  }

  /**
   * Set transitions form field settings.
   *
   * @param mixed|array $transitions
   *   Transition array.
   *
   * @return $this|mixed
   *   Return this.
   */
  public function setTransitions($transitions) {
    $this->transitions = $transitions;
    return $this;
  }

  /**
   * Set transition to execute.
   *
   * @param mixed|array $transition
   *   Transition array.
   *
   * @return $this
   *   Return this.
   */
  public function setTransition($transition) {
    $this->transition = $transition;
    return $this;
  }

  /**
   * Get transition after method transition.
   *
   * @return mixed|array
   *   Array of transition.
   */
  public function getTransition() {
    return $this->transition;
  }

  /**
   * Get transitions after method getPossibleTransitions.
   *
   * @return array
   *   Array of transition.
   */
  public function getTransitions() {
    return $this->transitions;
  }

  /**
   * Get transition if we know from a state and destination state.
   *
   * @param string $fromState
   *   From state.
   * @param string $toState
   *   Destination state.
   *
   * @return $this|mixed
   *   Return this.
   */
  public function transition($fromState, $toState) {
    foreach ($this->transitions as $transition) {
      if ($transition['to'] === $toState && in_array($fromState, $transition['from'])) {
        $this->setTransition($transition);
        break;
      }
    }
    return $this;
  }

  /**
   * Run transitions.
   *
   * @param object|mixed $entity
   *   Entity object.
   * @param null $field_name
   *   Field name.
   *
   * @return $this|mixed
   *   Return this or null.
   */
  public function applyTransition(&$entity = NULL, $field_name = NULL): mixed {
    if (!empty($this->transition) && $this->isTransitionAllowed($this->transition, $entity)) {
      $this->transition['field_name'] = $field_name;
      if (!empty($this->transition['workflow'])) {
        $workflows = is_array($this->transition['workflow']) ? $this->transition['workflow'] : [$this->transition['workflow']];
        foreach ($workflows as $workflowId) {
          $plugin = $this->workflow->createInstance($workflowId);
          if ($plugin && method_exists($plugin, 'action')) {
            $plugin->action($this->transition, ['transitions' => $this->transitions], $entity);
          }
        }
      }

      if (!empty($this->transition['action'])) {
        $actions = is_array($this->transition['action']) ? $this->transition['action'] : [$this->transition['action']];
        foreach ($actions as $actionId) {
          $plugin = $this->action->createInstance($actionId);
          if ($plugin && method_exists($plugin, 'execute')) {
            $plugin->execute($entity, $this->transition, ['transitions' => $this->transitions]);
          }
        }
      }
    }
    return $this;
  }

  /**
   * Check transition is allowed to execute.
   *
   * {@inheritdoc}
   */
  public function isTransitionAllowed(&$transition = NULL, $entity = NULL) {
    if (empty($transition)) {
      $transition = $this->getTransition();
    }
    // Check guard with id plugin guard.
    if (!empty($transition['guard'])) {
      $plugin = $this->guard->createInstance($transition['guard']);
      if ($plugin && method_exists($plugin, 'allowed')) {
        return $plugin->allowed($transition, ['transitions' => $this->getTransitions()], $entity);
      }
    }
    // Check permission.
    if (!empty($transition['permission'])) {
      $permissions = explode(',', trim($transition['permission']));
      if (count($permissions) > 1) {
        foreach ($permissions as $permission) {
          if ($this->currentUser->hasPermission(trim($permission))) {
            return TRUE;
          }
        }
      }
      else {
        return $this->currentUser->hasPermission($transition['permission']);
      }
    }
    elseif ($this->currentUser->hasPermission('access states')) {
      return TRUE;
    }
    // Check group.
    if (!empty($transition['group'])) {
      $groups = is_array($transition['group']) ? $transition['group'] : [$transition['group']];
      foreach ($groups as $group) {
        $inGroup = FALSE;
        // @phpstan-ignore-next-line
        $storageGroup = Group::load($group);
        $members = $storageGroup->getMembers();
        foreach ($members as $member) {
          $user = $member->getUser();
          if ($this->currentUser->id() == $user->id()) {
            return TRUE;
          }
        }
      }
      // @phpstan-ignore-next-line
      if (isset($inGroup)) {
        return $inGroup;
      }
    }
    // Check role.
    $userRoles = $this->currentUser->getRoles();
    if (in_array('administrator', $userRoles)) {
      return TRUE;
    }
    if (!empty($transition['role'])) {
      $workflowRoles = is_array($transition['role']) ? $transition['role'] : [$transition['role']];
      $intersection = array_intersect($workflowRoles, $userRoles);
      return !empty($intersection);
    }
    return FALSE;
  }

  /**
   * Set transitions by field.
   *
   * {@inheritdoc}
   */
  public function setTransitionByField($entity, $field_name) {
    $fieldDefinition = $entity->getFieldDefinition($field_name);
    $workflow = trim($fieldDefinition->getSetting('workflows'));
    if (!empty($workflow)) {
      $workflow = Yaml::decode($workflow);
      if (!empty($workflow['transitions'])) {
        $this->transitions = $workflow['transitions'];
      }
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPossibleTransitions($state) {
    $possible_transitions = array_filter($this->transitions, function ($transition) use ($state) {
      return is_array($transition) && in_array($state, $transition['from']);
    });
    return $this->setTransitions($possible_transitions);
  }

}
