(function ($, Drupal, once) {
  Drupal.behaviors.aceEditor = {
    attach: function (context) {
      $(once('json-yaml-editor', '.yaml-editor', context)).each(function () {
        let editorID = $(this).data('editor');
        if (mermaid) {
          if ($('#edit-settings-editor').attr('open')) {
            setTimeout(() => {
              $('#edit-settings-editor').removeAttr('open');
            }, 500);
          }
          let stateMachineString = $('#stateDiagram').text();
          if (stateMachineString !== '') {
            mermaid.init();
          }
          mermaid.initialize({startOnLoad: true});
          window.addEventListener('load', function () {
            let svgs = d3.selectAll(".mermaid svg");
            svgs.each(function () {
              let svg = d3.select(this);
              svg.html("<g>" + svg.html() + "</g>");
              let inner = svg.select("g");
              const initialWidth = svg.node().getBoundingClientRect().width;
              const initialHeight = svg.node().getBoundingClientRect().height;
              let zoom = d3.zoom().on("zoom", function (event) {
                const {k} = event.transform;
                inner.attr("transform", event.transform);
                svg.attr("width", initialWidth * k)
                  .attr("height", initialHeight * k);
              });
              svg.call(zoom);
            });
          });
        }
        if (ace) {
          let $textarea = $(this);
          let editor = ace.edit(editorID);

          if ($textarea.val() !== '') {
            editor.setValue($textarea.val());
          }
          editor.getSession().setMode("ace/mode/yaml");
          editor.focus();
          editor.getSession().setTabSize(2);
          editor.setTheme('ace/theme/chrome');
          editor.setOptions({
            minLines: 5,
            maxLines: 30
          });

          // Update Drupal textarea value.
          editor.getSession().on('change', function () {
            let yamlString = editor.getSession().getValue();
            $textarea.val(yamlString);

            try {
              const stateMachine = jsyaml.load(yamlString);
              if (stateMachine) {
                let mermaidString = mermaidConvertTransition(stateMachine);
                $('#stateDiagram').text(mermaidString).removeAttr('data-processed');
                mermaid.init();
              }
            } catch (e) {
              console.error(Drupal.t('YAML is not validate:'), e.message);
            }
          });
          // EventListener
          $textarea[0].addEventListener('statediagram', () => {
            editor.setValue($textarea.val());
          });
        }
      });
    }
  };

  function mermaidConvertTransition(stateMachine) {
    const stateDiagram = ["stateDiagram-v2"];
    const defaultState = '[*]';
    const transitions = stateMachine.transitions;
    const states = stateMachine.states;
    Object.keys(states).forEach(key => {
      let state = states[key];
      let stateName = key.replace(/[^a-zA-Z0-9_*\[\]]/g, '_');
      let stateLabel = state;
      if (state && state.label) {
        stateLabel = state.label;
      }
      stateDiagram.push(stateName + ':' + stateLabel.replace(/:/g, ''));
    });

    Object.entries(transitions).forEach(([key, transition]) => {
      const {from, to, label} = transition;
      if (Array.isArray(from)) {
        from.forEach((state) => {
          let currentState = state || defaultState;
          currentState = currentState.replace(/[^a-zA-Z0-9_*\[\]]/g, '_');
          let nextState = to.replace(/[^a-zA-Z0-9_*\[\]]/g, '_');
          if (nextState === '' || nextState === '_') {
            nextState = defaultState;
          }
          if (currentState === '_') {
            currentState = defaultState;
          }
          let query = `${currentState} --> ${nextState}`;
          if (label) {
            query += `: ${label}`;
          }
          stateDiagram.push(query);
        });
      }
    });
    return stateDiagram.join("\n");
  }
}(jQuery, Drupal, once));
