import { useRef } from 'react';
import StateMachineDiagram from './components/StateMachineDiagram';
import config from './config';
import yaml from 'js-yaml';
import './App.css';
import './config';

function App() {
  const svgRef = useRef(null);
  const content = document.getElementById(config.output);
  const dataField = yaml.load(content.value);
  const dataSetting = window.drupalSettings?.field_states?.diagram || [];
  const dataDefault = window.drupalSettings?.field_states?.default_value || null;
  let states = { ...dataField.states };

  /**
   * Normalize transitions.
   * Sometimes we need to convert from array of multiple values into a string.
   */
  let normalizeTransitions = (transitions) => {
    return transitions.flatMap((transition) => {
      const { from, to, id } = transition;

      if (Array.isArray(from)) {
        return from.map((fromValue) => {
          const stateFrom = fromValue || ' ';
          const stateTo = to || ' ';
          states[stateFrom] = states[stateFrom] || dataField.states[stateFrom] || ' ';
          states[stateTo] = states[stateTo] || dataField.states[stateTo] || ' ';

          return {
            ...transition,
            id: id || `${fromValue}_${to}`.replaceAll(" ", "_"),
            from: fromValue,
          };
        });
      } else {
        const stateFrom = from || ' ';
        const stateTo = to || ' ';
        states[stateFrom] = states[stateFrom] || dataField.states[stateFrom] || ' ';
        states[stateTo] = states[stateTo] || dataField.states[stateTo] || ' ';

        return {
          ...transition,
          id: id || `${from}_${to}`.replaceAll(" ", "_"),
        };
      }
    });
  };

  const transitions = normalizeTransitions(Object.values(dataField.transitions || []));
  return (
    <>
      {Object.keys(dataField.states).length > 0 && (<StateMachineDiagram states={states} transitions={transitions} nodeSpacing={config.nodeSpacing} edgeSpacing={config.edgeSpacing} editState={config.editState} dataSetting={dataSetting} dataDefault={dataDefault} svgRef={svgRef} />)}
    </>
  )
}

export default App;
