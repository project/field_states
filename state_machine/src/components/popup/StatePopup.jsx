import { memo } from 'react';
import './Popup.css';

const StatePopup = ({ state, onChange, onSave, onClose }) => {

  return (
    <div className="popup">
      <div className="popup-content">
        <div className='popup-title'>Edit State</div>
        <div className="popup-row">
          <label htmlFor="inputLabel" className="popup-label">Label</label>
          <div className="popup-input">
            <input type="text" className="form-control" id="inputLabel" name="label" value={state} onChange={(e) => { onChange(e, 'state') }} />
          </div>
        </div>
        <div className="popup-row">
          <div className="popup-operation">
            <button className='btn-success' onClick={() => { onSave('state') }}>Save</button>
          </div>
          <div className="popup-operation">
            <button className='btn-danger' onClick={onClose}>Close</button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default memo(StatePopup);
