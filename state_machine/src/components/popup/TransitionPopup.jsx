import { useState, memo } from 'react';
import './Popup.css';

const TransitionPopup = ({ transition, dataSetting, onChange, onSave, onDelete, onClose }) => {

  const [actions, setActions] = useState(transition.action || ['']);
  const [roles, setRoles] = useState(transition.role || ['']);
  const [workflows, setWorkflows] = useState(transition.workflow || ['']);

  const addAction = () => {
    setActions([...actions, '']);
  };

  const addWorkflow = () => {
    setWorkflows([...workflows, '']);
  };

  const addRole = () => {
    setRoles([...roles, '']);
  };

  const handleActionChange = (index, event) => {
    const newActions = [...actions];
    newActions[index] = event.target.value;
    setActions(newActions);
    onChange({ target: { name: 'action', value: newActions } }, 'transition');
  };

  const handleWorkflowChange = (index, event) => {
    const newWorkflows = [...workflows];
    newWorkflows[index] = event.target.value;
    setWorkflows(newWorkflows);
    onChange({ target: { name: 'workflow', value: newWorkflows } }, 'transition');
  };

  const handleRoleChange = (index, event) => {
    const newRoles = [...roles];
    newRoles[index] = event.target.value;
    setRoles(newRoles);
    onChange({ target: { name: 'role', value: newRoles } }, 'transition');
  };

  return (
    <div className="popup">
      <div className="popup-content">
        <div className='popup-title'>Edit Transition</div>
        <div className="popup-row">
          <div className="popup-group">
            <label htmlFor="inputLabel" className="popup-label">Label</label>
            <div className="popup-input">
              <input type="text" className="form-control" id="inputLabel" name="label" value={transition.label} onChange={(e) => { onChange(e, 'transition') }}
              />
            </div>
          </div>
          <div className="popup-group">
            <label htmlFor="inputGroup" className="popup-label">Group</label>
            <div className="popup-input">
              <input type="text" className="form-control" id="inputGroup" name="group" value={transition.group || ''} onChange={(e) => { onChange(e, 'transition') }} />
            </div>
          </div>
        </div>
        <div className="popup-row">
          <div className="popup-group">
            <label htmlFor="inputId" className="popup-label">Id</label>
            <div className="popup-input">
              <input type="text" className="form-control" id="inputId" name="id" value={transition.id} onChange={(e) => { onChange(e, 'transition') }}
              />
            </div>
          </div>
          <div className='popup-group popup-select'>
            <div className='popup-select-header'>
              <label className='popup-label'>Guard</label>
            </div>
            <div className='popup-select-list'>
              <div className="popup-input">
                <select value={transition.guard} name='guard' onChange={(e) => { onChange(e, 'transition') }}>
                  {transition.guard && dataSetting.guards && <option value={transition.guard} defaultChecked hidden disabled>{dataSetting.guards[transition.guard] || 'Not found'}</option>}
                  <option value=''>Choose guard</option>
                  {dataSetting.guards && Object.keys(dataSetting.guards).map((key) => (
                    <option key={key} value={key}>
                        {dataSetting.guards[key]}
                    </option>
                  ))}
                </select>
              </div>
            </div>
          </div>
        </div>
        <div className="popup-row group-3">
          <div className='popup-group popup-select'>
            <div className='popup-select-header'>
              <label className='popup-label'>Action</label>
              <button className="popup-select-btn" type="button" title="Add action" onClick={addAction}>+</button>
            </div>
            <div className='popup-select-list'>
              {actions.map((action, index) => (
                <div className="popup-input" key={index}>
                  <select value={action} onChange={(event) => handleActionChange(index, event)}>
                    {action && dataSetting.actions && <option value={action} defaultChecked hidden disabled>{dataSetting.actions[action] || 'Not found'}</option>}
                    <option value=''>Choose action</option>
                    {dataSetting.actions && Object.keys(dataSetting.actions).map((key) => (
                      <option key={key} value={key}>
                         {dataSetting.actions[key]}
                      </option>
                    ))}
                  </select>
                </div>
              ))}
            </div>
          </div>
          <div className='popup-group popup-select'>
            <div className='popup-select-header'>
              <label className='popup-label'>Workflow</label>
              <button className="popup-select-btn" type="button" onClick={addWorkflow}>+</button>
            </div>
            <div className='popup-select-list'>
              {workflows.map((workflow, index) => (
                <div className="popup-input" key={index}>
                  <select value={workflow} onChange={(event) => handleWorkflowChange(index, event)}>
                    {workflow && dataSetting.workflows && <option value={workflow} defaultChecked hidden disabled>{dataSetting.workflows[workflow] || 'Not found'}</option>}
                    <option value=''>Choose workflow</option>
                    {dataSetting.workflows && Object.keys(dataSetting.workflows).map((key) => (
                      <option key={key} value={key}>
                         {dataSetting.workflows[key]}
                      </option>
                    ))}
                  </select>
                </div>
              ))}
            </div>
          </div>
          <div className='popup-group popup-select'>
            <div className='popup-select-header'>
              <label className='popup-label'>Role</label>
              <button className="popup-select-btn" type="button" title="Add role" onClick={addRole}>+</button>
            </div>
            <div className='popup-select-list'>
              {roles.map((role, index) => (
                <div className="popup-input" key={index}>
                  <select value={role} onChange={(event) => handleRoleChange(index, event)}>
                    {role && dataSetting.roles && <option value={role} defaultChecked hidden disabled>{dataSetting.roles[role] || 'Not found'}</option>}
                    <option value=''>Choose role</option>
                    {dataSetting.roles && Object.keys(dataSetting.roles).map((key) => (
                      <option key={key} value={key}>
                         {dataSetting.roles[key]}
                      </option>
                    ))}
                  </select>
                </div>
              ))}
            </div>
          </div>
        </div>
        <div className="popup-row">
          <div className="popup-group">
            <label htmlFor="inputPermission" className="popup-label">Permission</label>
            <div className="popup-input">
              <input type="text" className="form-control" id="inputPermission" name="permission" value={transition.permission || ''} onChange={(e) => { onChange(e, 'transition') }} />
              <div className="description form-text">Separed by ,</div>
            </div>
          </div>
          <div className="popup-group">
            <label htmlFor="inputClass" className="popup-label">Class</label>
            <div className="popup-input">
              <input type="text" className="form-control" id="inputClass" name="class" value={transition.class || ''} onChange={(e) => { onChange(e, 'transition') }} />
            </div>
          </div>
        </div>
        <div className="popup-row">
          <div className="popup-operation">
            <button className='btn-success' onClick={onSave}>Save</button>
          </div>
          <div className="popup-operation">
            <button className='btn-danger' onClick={onDelete}>Delete</button>
          </div>
        </div>
        <button onClick={onClose} className="close-btn">X</button>
      </div>
    </div>
  );
};

export default memo(TransitionPopup);
