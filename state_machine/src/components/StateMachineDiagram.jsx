import { memo, useCallback, useEffect, useState } from 'react';
import * as d3 from 'd3';
import dagre from 'dagre';
import yaml from 'js-yaml'
import config from '../config'
import TransitionPopup from './popup/TransitionPopup';
import StatePopup from './popup/StatePopup';
import DiagramTool from './DiagramTool';

const StateMachineDiagram = ({ states, transitions, nodeSpacing, edgeSpacing, editState, dataSetting, dataDefault, svgRef }) => {

  const [infoBox, setInfoBox] = useState({ display: 'none', x: 0, y: 0 });
  const [keyState, setKeyState] = useState(null); // Key state on modify.
  const [popupState, setPopupState] = useState(null); // Popup when click on state.
  const [popupTransition, setPopupTransition] = useState(null); // Popup when click on transition

  const [currentStates, setCurrentStates] = useState(states); // Save states.
  const [currentTransitions, setCurrentTransitions] = useState(transitions); // State transitions

  const [scale, setScale] = useState(1);
  const [translate, setTranslate] = useState(0);
  const [widthSvg, setWidthSvg] = useState(0);
  const [heightSvg, setHeightSvg] = useState(0);
  const [selectedFromState, setSelectedFromState] = useState(null); // State selected "from".
  const [dragLine, setDragLine] = useState(null); // Tempo line.
  const [isDrawing, setIsDrawing] = useState(false); // Draw tempo line when mouse move.
  const [direction, setDirection] = useState(localStorage.getItem(config.fieldName) || "TB"); // Direction.
  const nodeHeight = 40;

  // Calculate the width of the text.
  const getTextWidth = (text) => {
    let span = document.createElement("span");
    span.style.font = "16px Arial";
    span.style.visibility = "hidden";
    span.style.position = "absolute";
    span.innerText = text;
    document.body.appendChild(span);
    let width = span.offsetWidth;
    document.body.removeChild(span);
    return width;
  };
  // Callback direction
  const handleDirection = useCallback((data) => {
    setDirection(data)
  },[])

  const handleScale = useCallback((data) => {
    setScale(data)
  },[])

  // The process selected "from"
  const handleStateClick = (d) => {
    if(!editState) return null;

    if (selectedFromState === null) {
      setSelectedFromState(d);
      setIsDrawing(true);
    } else {
      if (selectedFromState !== d && !currentTransitions.some(item => item.from === selectedFromState && item.to === d)) {
        const updatedTransitions = [...currentTransitions, {
          id: `${selectedFromState}_${d}`.replaceAll(' ', '_'),
          label: `${states[selectedFromState]} → ${states[d]}`.trim(),
          from: selectedFromState.trim() || '',
          to: d.trim() || '',
        }];
        setCurrentTransitions(updatedTransitions);
      }
      setSelectedFromState(null);
      setIsDrawing(false);
      if (dragLine) {
        dragLine.remove();
        setDragLine(null);
      }
    }
  };

  // Update the position of the gray line as the mouse moves.
  const handleMouseMove = (event, translate) => {
    if(!editState) return null;

    if (isDrawing && dragLine) {
      const svg = svgRef.current;
      const svgRect = svg.getBoundingClientRect(); // Get the position and size of the SVG in the page

      // Calculate mouse position relative to SVG, add translateX to compensate for graph displacement
      let mouseX = (event.clientX - svgRect.left) / scale;
      let mouseY = (event.clientY - svgRect.top) / scale;

      if(direction === 'TB' || direction === 'BT') {
        mouseX -= translate;
      } else {
        mouseY -= translate * 2;
      }

      dragLine
        .attr('x2', mouseX)
        .attr('y2', mouseY);
    }
  };

  // Cancel drag when pressing Escape
  const handleKeyDown = useCallback((event) => {
    if(!editState) return null;

    if (event.key === 'Escape' && isDrawing) {
      setSelectedFromState(null); // Cancel state "from".
      if (dragLine) {
        dragLine.remove();
        setDragLine(null);
      }
      setIsDrawing(false);
    }
  }, [isDrawing, dragLine, editState]);

  useEffect(() => {
     // Listener pressing "Escape"
    document.addEventListener('keydown', handleKeyDown);
    return () => {
      document.removeEventListener('keydown', handleKeyDown);
    };
  }, [isDrawing, dragLine, handleKeyDown]);

  useEffect(() => {
    const g = new dagre.graphlib.Graph({ multigraph: true });
    const colors = ['blue', 'green', 'orange', 'red', 'purple', 'aqua', 'black'];
    const colorMap = {
      primary: 'blue',
      success: 'green',
      warning: 'orange',
      danger: 'red',
      secondary: 'purple',
      info: 'aqua',
      dark: 'black'
    };

    g.setGraph({
      ranksep: edgeSpacing,
      nodesep: nodeSpacing,
      rankdir: direction,
    });
    g.setDefaultEdgeLabel(() => ({}));

    // Add node root
    const handleAddRoot = () => {
      const rootValue = dataDefault || ' '
      !dataDefault && !g.setNode(" ", { label: '', width: 20, height: 20});
      return rootValue;
    }

    // Add node to diagram
    Object.keys(currentStates).forEach((key) => {
      const textWidth = getTextWidth(currentStates[key]) + 20;
      g.setNode(key, { label: currentStates[key], width: textWidth, height: nodeHeight });
    });

    // Add edge to diagram.
    const transitionKeys = Object.keys(currentTransitions);
    transitionKeys.forEach((key, index) => {
      const { label, to } = currentTransitions[key];
      const from = currentTransitions[key].from.trim().length === 0 ? handleAddRoot() : currentTransitions[key].from
      let color = colors[index % colors.length];
      if(currentTransitions[key].class) {
        for (let keyClass in colorMap) {
          if (currentTransitions[key].class.includes(keyClass)) {
            color = colorMap[keyClass];
            break;
          }
        }
      }
      g.setEdge(from, to, { label, color, key });
    });

    dagre.layout(g);

    // Calculate the actual size of the graph
    let minGraphX = Math.min(...g.nodes().map((n) => g.node(n).x - g.node(n).width / 2)) - 20;
    let maxGraphX = Math.max(...g.nodes().map((n) => g.node(n).x + g.node(n).width / 2)) + 20;
    let minGraphY = Math.min(...g.nodes().map((n) => g.node(n).y - g.node(n).height / 2)) - 20;
    let maxGraphY = Math.max(...g.nodes().map((n) => g.node(n).y + g.node(n).height / 2)) + 20;

    // Check the transitions to see if the width is different from the button.
    g.edges().forEach((e) => {
      const points = g.edge(e).points;
      const textWidth = getTextWidth(g.edge(e).label) + 20;
      const midPoint = points[Math.floor(points.length / 2)];

      // Check the transition midpoint to ensure the label does not exceed the limit.
      minGraphX = Math.min(minGraphX, midPoint.x - textWidth);
      maxGraphX = Math.max(maxGraphX, midPoint.x + textWidth);
      minGraphY = Math.min(minGraphY, midPoint.y - 40);
      maxGraphY = Math.max(maxGraphY, midPoint.y + 40);
    });

    // If minGraphX is negative.
    const mouseX = minGraphX < 0 ? Math.abs(minGraphX) : 0;

    minGraphX = Math.max(0, minGraphX);

    // Update size of SVG
    const svgWidth = maxGraphX - minGraphX + translate;
    const svgHeight = maxGraphY - minGraphY;

    const svg = d3.select(svgRef.current);
    svg.attr('width', svgWidth).attr('height', svgHeight);
    svg.selectAll('*').remove();

    // Zoom SVG
    const gElement = svg.append('g').attr('transform', `translate(${translate},${-minGraphY})`);
    const zoom = d3.zoom().on('zoom', (event) => {
      const translateX = event.transform.x + translate;
      const translateY = event.transform.y - minGraphY;
      gElement.attr('transform', `translate(${translateX},${translateY})`);
    });
    svg.call(zoom).on("wheel.zoom", null);
    svg.on("wheel", (event) => {
      if (event.ctrlKey) {
        event.preventDefault();
        zoom.scaleBy(svg.transition().duration(0), 1 + (event.deltaY * -0.001));
      }
    });

    setTranslate(mouseX);
    setWidthSvg(svgWidth);
    setHeightSvg(svgHeight);

    // Create marker and arrow color.
    gElement.append('defs')
      .selectAll('marker')
      .data(g.edges())
      .enter()
      .append('marker')
      .attr('key', (d) => g.edge(d).key)
      .attr('id', (d) => `arrowhead-${g.edge(d).key}`)
      .attr('viewBox', '-0 -5 10 10')
      .attr('refX', 12)
      .attr('refY', 0)
      .attr('orient', 'auto')
      .attr('markerWidth', 6)
      .attr('markerHeight', 6)
      .attr('xoverflow', 'visible')
      .append('svg:path')
      .attr('d', 'M 0,-5 L 10 ,0 L 0,5')
      .attr('fill', (d) => g.edge(d).color)
      .style('stroke', 'none');

    const nodes = gElement
      .append('g')
      .selectAll('rect')
      .data(g.nodes())
      .enter()
      .append('g')
      .attr('key', (d) => d)
      .attr('transform', (d) => {
        const node = g.node(d);
        return `translate(${node.x - node.width / 2},${node.y - node.height / 2})`;
      })
      .style('cursor', 'pointer')
      .on('click', (event, d) => {
        selectedFromState !== d ? handleStateClick(d) : handleOpenPopup(d, 'state')
      })

    // Square for state
    nodes.each(function(d) {
      if (g.node(d).label.trim().length === 0) {
        !dataDefault && d3.select(this)
          .append('circle')
          .attr('cx', g.node(d).width / 2)
          .attr('cy', g.node(d).height / 2)
          .attr('r', Math.min(g.node(d).width, g.node(d).height) / 2)
          .attr('fill', 'black')
          .attr('stroke', 'black');
      } else {
        d3.select(this)
          .append('rect')
          .attr('width', g.node(d).width)
          .attr('height', g.node(d).height)
          .attr('fill', selectedFromState === d ? 'lightgreen' : 'lightblue')
          .attr('stroke', 'black')
          .attr('rx', 5)
          .attr('ry', 5);
      }
    });
    // Add label for state
    nodes
      .append('text')
      .attr('x', (d) => g.node(d).width / 2)
      .attr('y', (d) => g.node(d).height / 2)
      .attr('dy', '.35em')
      .attr('text-anchor', 'middle')
      .text((d) => g.node(d).label);

    // Click on state, mark "from" and draw tempo line
    if (selectedFromState) {
      const fromNode = g.node(selectedFromState);
      const line = gElement.append('line')
        .attr('stroke', 'gray')
        .attr('stroke-dasharray', '5,5')
        .attr('x1', fromNode.x)
        .attr('y1', fromNode.y)
        .attr('x2', fromNode.x)
        .attr('y2', fromNode.y);
      setDragLine(line); // Update line when mouse moving.
    }

    const edges = gElement
      .append('g')
      .selectAll('path')
      .data(g.edges())
      .enter()
      .append('g');

    // Draw curve Bézier.
    edges
      .append('path')
      .attr('d', (d) => {
        const points = g.edge(d).points;
        const path = d3.line()
          .curve(d3.curveBasis)
          .x((p) => p.x)
          .y((p) => p.y);
        return path(points);
      })
      .attr('fill', 'none')
      .attr('stroke', (d) => g.edge(d).color)
      .attr('marker-end', (d) => `url(#arrowhead-${g.edge(d).key})`);

    // Draw transition text.
    edges
      .append('rect')
      .attr('x', (d) => {
        const points = g.edge(d).points;
        const midPoint = points[Math.floor(points.length / 2)];
        const textWidth = getTextWidth(g.edge(d).label) + 10;
        return midPoint.x - textWidth / 2;
      })
      .attr('y', (d) => {
        const points = g.edge(d).points;
        const midPoint = points[Math.floor(points.length / 2)];
        return midPoint.y - 15;
      })
      .attr('width', (d) => getTextWidth(g.edge(d).label) + 10)
      .attr('height', 30)
      .attr('class', 'edge-label')
      .attr('rx', 10)
      .attr('ry', 10)
      .attr('fill', (d) => g.edge(d).color)
      .style('cursor', 'pointer')
      .on('mouseout', handleMouseOut)
      .on('mouseover', (event, d) => handleMouseOver(event, g.edge(d).key))
      .on('click', (event, d) => {
        handleOpenPopup(g.edge(d).key, 'transition')
      });

    edges
      .append('text')
      .attr('x', (d) => {
        const points = g.edge(d).points;
        const midPoint = points[Math.floor(points.length / 2)];
        return midPoint.x;
      })
      .attr('y', (d) => {
        const points = g.edge(d).points;
        return points[Math.floor(points.length / 2)].y;
      })
      .attr('dy', '.35em')
      .attr('text-anchor', 'middle')
      .attr('fill', 'white')
      .text((d) => g.edge(d).label)
      .style('font-size', '14px')
      .style('cursor', 'pointer')
      .on('mouseout', handleMouseOut)
      .on('mouseover', (event, d) => handleMouseOver(event, g.edge(d).key))
      .on('click', (event, d) => {
        handleOpenPopup(g.edge(d).key, 'transition')
      });
  }, [currentStates, currentTransitions, direction, translate, svgRef, nodeSpacing, edgeSpacing, selectedFromState, isDrawing]);

  useEffect(() => {
    const content = document.getElementById(config.output);
    const newTransitions = {};

    currentTransitions.forEach(transition => {
      const { from, to, ...rest } = transition;
      const newKey = `${from}_${to}`.trim().replace(/^_/, '').replaceAll(" ", "_");

      newTransitions[newKey] = {
        from: [from],
        to,
        ...rest
      };
    });

    if (content) {
      let saveStates = { ...currentStates };
      Object.keys(saveStates).filter(key => !key.trim()).forEach(key => delete saveStates[key]);
      content.value = yaml.dump({ states: saveStates, transitions: newTransitions });
      const event = new Event('statediagram', { bubbles: true, cancelable: true });
      content.dispatchEvent(event);
    }
  }, [currentStates, currentTransitions])

  const handleOpenPopup = (value, type) => {
    if (type === 'state') {
      setKeyState(value);
      setPopupState(currentStates[value]);
      setPopupTransition(null);
    } else {
      setPopupTransition(currentTransitions[value]);
      setPopupState(null);
      setKeyState(null);
    }
    setIsDrawing(false);
  }

  const handleChangePopup = (e, type) => {
    let { name, value } = e.target
    if (type === 'state') {
      if(value !== '') {
        setPopupState(value);
      }
    } else {
      if(name === 'label' && value === '') {
        return null;
      } else {
        if(typeof value === 'object') {
          const newValue = [...new Set(value.filter(action => action))];
          value = newValue.length > 0 ? newValue : [''];
        }
        setPopupTransition((prev) => ({ ...prev, [name]: value }));
      }
    }
  }

  const handleSavePopup = (type) => {
    if (type === 'state') {
      currentStates[keyState] = popupState;
      setCurrentStates({ ...currentStates });
      setPopupState(null);
      setKeyState(null);
      setSelectedFromState(null);
    } else {
      const updatedTransitions = currentTransitions.map((t) =>
        t.from === popupTransition.from && t.to === popupTransition.to ? popupTransition : t
      );
      setCurrentTransitions(updatedTransitions);
      setPopupTransition(null);
    }
  };

  const handleClosePopup = () => {
    setKeyState(null);
    setPopupState(null);
    setPopupTransition(null);
    setSelectedFromState(null);
  };

  const handleDeleteTransition = () => {
    const updatedTransitions = currentTransitions.filter(
      (t) => t.from !== popupTransition.from || t.to !== popupTransition.to
    );
    setCurrentTransitions(updatedTransitions);
    setPopupTransition(null);
  }

  const handleMouseOver = (event, key) => {
    let data = {}
    const value = currentTransitions[key]

    const { permission, action, role, workflow, guard } = value;

    if(permission) {
      data['permission'] = permission
    }

    const addToData = (key, value) => {
      if (!value) return;

      if (Array.isArray(value) && value.length) {
        const out = value.map(item => (dataSetting[key]?.[item] ?? item));
        data[key] = out.join(', ');
      } else {
        data[key] = dataSetting[key]?.[value];
      }
    };

    addToData('actions', action);
    addToData('roles', role);
    addToData('workflows', workflow);
    addToData('guards', guard);

    setInfoBox({
      display: 'block',
      x: event.clientX + 10,
      y: event.clientY + 10,
      count: Object.keys(data).length,
      datas: data
    });
  };

  const handleMouseOut = () => {
    setInfoBox({ ...infoBox, display: 'none' });
  };

  return (
    <>
      <DiagramTool handleDirection={handleDirection} handleScale={handleScale} svgRef={svgRef}/>
      <div className='content-diagram-svg'>
        <div id='svg-diagram' style={{width: widthSvg, height: heightSvg}}>
          <svg onMouseMove={(e) => handleMouseMove(e, translate)} ref={svgRef}></svg>
        </div>
        {infoBox.count > 0 && (<div id="info-box" style={{ display: infoBox.display, left: infoBox.x, top: infoBox.y}} >
          {infoBox.datas?.permission && <span style={{display: 'block'}}><strong>Permission: </strong>{infoBox.datas.permission}</span>}
          {infoBox.datas?.actions && <span style={{display: 'block'}}><strong>Actions: </strong>{infoBox.datas.actions}</span>}
          {infoBox.datas?.roles && <span style={{display: 'block'}}><strong>Roles: </strong>{infoBox.datas.roles}</span>}
          {infoBox.datas?.workflows && <span style={{display: 'block'}}><strong>Workflow: </strong>{infoBox.datas.workflows}</span>}
          {infoBox.datas?.guards && <span style={{display: 'block'}}><strong>Guards: </strong>{infoBox.datas.guards}</span>}
        </div>)}
      </div>

      {/* Popup click on label transition*/}
      {editState && popupTransition && (
        <TransitionPopup
          transition={popupTransition}
          dataSetting={dataSetting}
          onChange={handleChangePopup}
          onSave={handleSavePopup}
          onClose={handleClosePopup}
          onDelete={handleDeleteTransition}
        />
      )}

      {/* Click on state popup to change label */}
      {editState && popupState && (
        <StatePopup
          state={popupState}
          currentStates={currentStates}
          onChange={handleChangePopup}
          onSave={handleSavePopup}
          onClose={handleClosePopup}
        />
      )}
    </>
  );
};

export default memo(StateMachineDiagram);
