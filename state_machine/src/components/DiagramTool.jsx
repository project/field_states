import { useState, memo } from 'react';
import { saveAs } from 'file-saver';
import { Canvg } from 'canvg';
import jsPDF from 'jspdf';
import config from '../config';

const DiagramTool = ({ handleDirection, handleScale, svgRef }) => {

  const direction = ['RL', 'BT', 'LR', 'TB']
  const [rotateIndex, setRotate] = useState(0);
  const [zoom, setZoom] = useState(1);

  // Save
  const saveToPng = async () => {
    const svgElement = svgRef.current;
    if (svgElement) {
      const canvas = document.createElement('canvas');
      const ctx = canvas.getContext('2d');
      const svgData = new XMLSerializer().serializeToString(svgElement);

      const v = await Canvg.fromString(ctx, svgData);
      await v.render();

      canvas.toBlob((blob) => {
        saveAs(blob, 'diagram.png');
      });
    }
  };

  const saveToPdf = async () => {
    const svgElement = svgRef.current;
    if (svgElement) {
      const canvas = document.createElement('canvas');
      const ctx = canvas.getContext('2d');
      const svgData = new XMLSerializer().serializeToString(svgElement);

      const v = await Canvg.fromString(ctx, svgData);
      await v.render();

      const imgData = canvas.toDataURL('image/png');
      const pdf = new jsPDF();
      pdf.addImage(imgData, 'PNG', 0, 0, canvas.width / 5, canvas.height / 5); // Chia tỷ lệ phù hợp
      pdf.save('diagram.pdf');
    }
  };

  const saveToSvg = () => {
    const svgElement = svgRef.current;
    if (svgElement) {
      const svgData = new XMLSerializer().serializeToString(svgElement);
      const blob = new Blob([svgData], { type: 'image/svg+xml;charset=utf-8' });
      saveAs(blob, 'diagram.svg');
    }
  };

  // Zoom
  const zoomSvgBig = () => {
    const svgElement = svgRef.current;
    if (svgElement) {
      let value = Number((zoom + 0.1).toFixed(1));
      svgElement.setAttribute('transform', `scale(${value}) translate(0, 0)`);
      setZoom(value);
      handleScale(value);
    }
  }

  const zoomSvgSmall = () => {
    const svgElement = svgRef.current;
    if (svgElement) {
      let value = zoom > 0.1 ? Number((zoom - 0.1).toFixed(1)) : 0;
      svgElement.setAttribute('transform', `scale(${value}) translate(0, 0)`);
      setZoom(value);
      handleScale(value);
    }
  }

  // Print
  const print = () => {
    const svgElement = svgRef.current;
    const printWindow = window.open("", "PRINT", "height=600,width=800");
    printWindow.document.write(`
          <html>
            <head><title>Print SVG</title></head>
            <body>${svgElement.outerHTML}</body>
          </html>`)
    printWindow.document.close();
    printWindow.focus();
    printWindow.print();
  }

  // Rotate
  const rotate = () => {
    const rotate = direction[rotateIndex];
    handleDirection(rotate);
    config.fieldName && localStorage.setItem(config.fieldName, rotate)
    setRotate(rotateIndex < direction.length - 1 ? rotateIndex + 1 : 0);
  }

  return (
    <div className="btn-group" role="group" aria-label="Diagram tool">
      <button type="button" className="btn btn-primary" onClick={rotate} title="Roatation">↻</button>
      <div className="btn btn-save btn-secondary" title="Save">
        <span>💾</span>
        <ul className="list-save">
          <li>
            <button type="button" className="btn btn-save-item btn-secondary" onClick={saveToPng} title="Save to png">🖼</button>
          </li>
          <li>
            <button type="button" className="btn btn-save-item btn-secondary" onClick={saveToPdf} title="Save to pdf">🗄</button>
          </li>
          <li>
            <button type="button" className="btn btn-save-item btn-secondary" onClick={saveToSvg} title="Save to svg">🖽</button>
          </li>
        </ul>
      </div>
      <button type="button" className="btn btn-info" onClick={() => zoomSvgBig()} title="Zoom +">🔍+</button>
      <button type="button" className="btn btn-info" onClick={() => zoomSvgSmall()} title="Zoom -">🔍-</button>
      <button type="button" className="btn btn-success" onClick={print} title="Print">🖶</button>
    </div>
  )
}

export default memo(DiagramTool);
