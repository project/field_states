const data = document.getElementById('edit-diagram')
const config = {
  nodeSpacing: data?.getAttribute('data-node-spacing'),
  edgeSpacing: data?.getAttribute('data-edge-spacing'),
  output: data?.getAttribute('data-output'),
  format: data?.getAttribute('data-format'),
  editState: data?.getAttribute('edit-state'),
  fieldName: data?.getAttribute('data-field-name'),
}

export default config;
