import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  build: {
    lib: {
      entry: 'src/main.jsx',
      name: 'StateMachine',
      fileName: (format) => `state-machine.${format}.js`,
    },
  },
  define: {
    'process.env': {}
  }
})
